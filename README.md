# Eye saver
Simple eye saver application
## Features
* blue filter
* brightness (uses xrandr)

## Installation
run `make install`

## Note:
* Gnome users must disable gsd-color.
```shell
cp -pf /etc/xdg/autostart/org.gnome.SettingsDaemon.Color.desktop{,.bak}
sed -i "s|Exec=.*|Exec=/bin/true|g" /etc/xdg/autostart/org.gnome.SettingsDaemon.Color.desktop
```
