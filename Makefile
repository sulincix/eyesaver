build:
	: run make insall
install:
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/usr/share/applications
	mkdir -p $(DESTDIR)/usr/share/eyesaver
	mkdir -p $(DESTDIR)/etc/xdg/autostart/
	install main.py $(DESTDIR)/usr/bin/org.sulincix.eyesaver
	install application.desktop $(DESTDIR)/usr/share/applications/org.sulincix.eyesaver.desktop
	install autostart.desktop $(DESTDIR)/etc/xdg/autostart/org.sulincix.eyesaver.desktop
	install main.ui $(DESTDIR)/usr/share/eyesaver
	install icon.svg $(DESTDIR)/usr/share/eyesaver
