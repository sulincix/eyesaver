#!/usr/bin/python3
import os, sys
import subprocess
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

if not os.path.exists("main.ui"):
    os.chdir("/usr/share/eyesaver")

builder = Gtk.Builder()
builder.add_from_file("main.ui")
window = builder.get_object("window")
window.connect("destroy",Gtk.main_quit)
window.set_title("Eye Saver")
window.show_all()

blue_filter_level = 100
brightness = 100
mode = "custom"

devices = subprocess.getoutput('LANG=C xrandr | grep " connected" | cut -f 1 -d " "').split("\n")

def save(widget):
    with open("{}/.config/eyesaver".format(os.environ["HOME"]),"w") as f:
        f.write("brightness={}\n".format(brightness))
        f.write("blue-filter={}\n".format(blue_filter_level))
        f.write("mode={}\n".format(mode))
    Gtk.main_quit()

def reset(widget):
    if mode == "custom":
        builder.get_object("blue_filter").set_value(100)
    builder.get_object("brightness").set_value(100)

def update():
    level = (100-float(blue_filter_level))
    for device in devices:
        if mode == "reading":
            command = ("xrandr --output {} --gamma 0.7:0.7:0.7 --brightness {}".format(
                device, float(brightness)/100
            ))
        elif mode == "gaming":
            command = ("xrandr --output {} --gamma 0.9:0.9:0.9 --brightness {}".format(
                device, float(brightness)/100
            ))
        elif mode == "nighttime":
            command = ("xrandr --output {} --gamma 1.1:0.8:0.7 --brightness {}".format(
                device, float(brightness)/100
            ))
        else:
            command = "xrandr --output {} --gamma 1:{}:{} --brightness {}".format(
                device,
                (100 - level* 0.5)/100,
                (100 - level)/100,
                float(brightness)/100
            )
        print(command)
        os.system(command)

def blue_filter_action(widget):
    global blue_filter_level
    blue_filter_level = int(widget.get_value())
    update()

def brightness_action(widget):
    global brightness
    brightness = int(widget.get_value())
    update()

def decrease_brightness(widget):
    builder.get_object("brightness").set_value(brightness-5)
def increase_brightness(widget):
    builder.get_object("brightness").set_value(brightness+5)

def decrease_blue_filter(widget):
    builder.get_object("blue_filter").set_value(blue_filter_level-5)
def increase_blue_filter(widget):
    builder.get_object("blue_filter").set_value(blue_filter_level+5)

if os.path.exists("{}/.config/eyesaver".format(os.environ["HOME"])):
    with open("{}/.config/eyesaver".format(os.environ["HOME"]),"r") as f:
        for line in f.read().split("\n"):
            if line.startswith("brightness"):
                brightness = float(line.split("=")[1].strip())
            elif line.startswith("blue-filter"):
                blue_filter_level = float(line.split("=")[1].strip())
            elif line.startswith("mode"):
                mode = line.split("=")[1].strip().lower()

def custom_action(widget):
    global mode
    builder.get_object("box_bluefilter").set_sensitive(True)
    builder.get_object("brightness").set_sensitive(True)
    builder.get_object("reset").set_sensitive(True)
    mode = "custom"
    builder.get_object("brightness").set_value(100)
    update()

def reading_action(widget):
    global mode
    global brightness
    builder.get_object("box_bluefilter").set_sensitive(False)
    builder.get_object("brightness").set_sensitive(False)
    builder.get_object("reset").set_sensitive(False)
    mode = "reading"
    builder.get_object("brightness").set_value(75)
    update()

def gaming_action(widget):
    global mode
    builder.get_object("box_bluefilter").set_sensitive(False)
    builder.get_object("brightness").set_sensitive(False)
    builder.get_object("reset").set_sensitive(False)
    mode = "gaming"
    builder.get_object("brightness").set_value(110)
    update()

def night_action(widget):
    global mode
    global brightness
    builder.get_object("box_bluefilter").set_sensitive(False)
    builder.get_object("brightness").set_sensitive(False)
    builder.get_object("reset").set_sensitive(False)
    mode = "nighttime"
    builder.get_object("brightness").set_value(45)
    update()


builder.get_object(mode).set_active(True)
print(mode)
update()
if sys.argv[-1] == "autostart":
    sys.exit(0)

builder.get_object("blue_filter").set_value(blue_filter_level)
builder.get_object("brightness").set_value(brightness)

builder.get_object("blue_filter").connect("value-changed",blue_filter_action)
builder.get_object("brightness").connect("value-changed",brightness_action)
builder.get_object("save").connect("clicked",save)
builder.get_object("reset").connect("clicked",reset)
builder.get_object("decrease_brightness").connect("clicked",decrease_brightness)
builder.get_object("increase_brightness").connect("clicked",increase_brightness)

builder.get_object("decrease_blue_filter").connect("clicked",decrease_blue_filter)
builder.get_object("increase_blue_filter").connect("clicked",increase_blue_filter)


builder.get_object("custom").connect("clicked",custom_action)
builder.get_object("reading").connect("clicked",reading_action)
builder.get_object("gaming").connect("clicked",gaming_action)
builder.get_object("nighttime").connect("clicked",night_action)


Gtk.main()
